package lecture06

import java.io.File


object PhoneMnemonics{

  val mnemonics = Map('0' -> "0+", '1' -> "1",
    '2' -> "2ABCÇ", '3' -> "3DEF",  '4' -> "4GHI", '5' -> "5JKL",
    '6' -> "6MNO", '7' -> "7PQRS", '8' -> "8TUV", '9' -> "9WXYZ");

  val charCode:Map[Char,Char] = for {
    (digit,chars) <- mnemonics
    char <- chars
  } yield (char -> digit)

  def wordCode(word:String):String = word.toUpperCase.filter(c => charCode.contains(c)) map charCode

  val wordsForNum:Map[String,Seq[String]] = dictionary groupBy wordCode withDefaultValue Seq()

  def average[T](seq:Seq[T])(f: T => Int) = seq.foldLeft(0)((acc,e) => acc+f(e)) / seq.length

  def encode(number:String):List[List[String]] = {
    if (number.isEmpty) List(List())
    else {
      for {
        split <- 1 to number.length
        word <- wordsForNum(number take split)
        rest <- encode(number drop split)
      } yield word :: rest
    }.toList.sortBy(_.length).sortBy(s => s.count(w => w.length == 1))
  }

  def translate(number:String):List[String] = 
    encode(number).map(s => s.mkString(" "))

  private def loadFile(filePath:List[String]):List[String] = {

    /**
     * Get a child of a file. For example,
     * 
     *   subFile(homeDir, "b", "c")
     * 
     * corresponds to ~/b/c
     */
    def subFile(file: File, children: String*) = {
      children.foldLeft(file)((file, child) => new File(file, child))
    }

    /**
     * Get a resource from the `src/main/resources` directory. Eclipse does not copy
     * resources to the output directory, then the class loader cannot find them.
     */
    def resourceAsStreamFromSrc(resourcePath: List[String]): Option[java.io.InputStream] = {
      val classesDir = new File(getClass.getResource(".").toURI)
      val projectDir = classesDir.getParentFile.getParentFile.getParentFile.getParentFile
      val resourceFile = subFile(projectDir, ("src" :: "main" :: "resources" :: resourcePath): _*)
      if (resourceFile.exists)
        Some(new java.io.FileInputStream(resourceFile))
      else
        None
    }


    val wordstream = Option {
      getClass.getClassLoader.getResourceAsStream(filePath.mkString("/"))
    } orElse {
      resourceAsStreamFromSrc(filePath)
    } getOrElse {
      sys.error("Could not load the file, file not found")
    }
    try {
      val s = io.Source.fromInputStream(wordstream)
      s.getLines.toList
    } catch {
      case e: Exception =>
        println("Could not load word list: " + e)
        throw e
    } finally {
      wordstream.close()
    }
  }

  lazy val dictionary = loadFile("paraules-ascii.txt"::Nil)

}