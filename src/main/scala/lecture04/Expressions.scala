package lecture04

object Expr{
  def eval(e:Expr):Int = e match {
    case Sum(e1,e2) => eval(e1) + eval(e2)
    case Product(e1,e2) => eval(e1) * eval(e2)
    case Var(name) => throw new UnsupportedOperationException("Not yet implemented")
    case Number(n) => n
  }

  def normalize(e:Expr):Expr = e match {
    case Sum(e1,e2) => Sum(normalize(e1),normalize(e2))
    case Product(Sum(e1,e2),e3) => normalize(e1 * e3 + e2 * e3)
    case Product(e1,Sum(e2,e3)) => normalize(e1 * e2 + e1 * e3) 
    case Product(e1,e2) => Product(normalize(e1),normalize(e2))
    case e => e
  }

  def show(e:Expr, depth:Int = 0):String = e match{
    case Sum(e1,e2) => (if(depth>0) "(" else "") + show(e1) + " + " + show(e2) + (if(depth>0) ")" else "")
    case Product(e1,e2) => show(e1,1) + " * " + show(e2,1)
    case Number(n) => ""+n
    case Var(x) => ""+x
  }
}

trait Expr{
  def + (e: Expr) = new Sum(this, e)
  def * (e: Expr) = new Product(this, e)
  // def simplify(e: Expr): Expr = throw new UnsupportedOperationException("Not yet implemented")
}

case class Sum(leftOp:Expr, rightOp: Expr) extends Expr

case class Product(leftOp:Expr, rightOp:Expr) extends Expr

case class Number(numValue: Int) extends Expr

case class Var(x:String) extends Expr