package lecture04

object List{
  def apply() = Nil
  def apply[T](elem:T):List[T] = new Cons(elem, Nil)
  def apply[T](elem1:T, elem2:T):List[T] = apply(elem2).cons(elem1)
}

trait List[+T]{
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
  def cons[S >: T] (elem:S): List[S] = new Cons(elem, this)
  def nth(pos:Int):T
  // = throw new UnsupportedOperationException("Not yet implemented!")
}

object Nil extends List[Nothing]{
  val isEmpty = true
  def head = throw new NoSuchElementException("Nil.head")
  def tail = throw new NoSuchElementException("Nil.tail")
  def nth(pos:Int) = throw new IndexOutOfBoundsException
}

class Cons[T](val head:T, val tail:List[T]) extends List[T]{
  val isEmpty = false
  def nth(pos:Int):T = 
    if(pos == 0) head
    else tail.nth(pos-1)
}
