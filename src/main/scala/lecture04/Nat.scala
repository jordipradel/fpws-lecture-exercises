package lecture04

import common._

abstract class Nat{

	def isZero: Boolean
	def predecessor: Nat
	def successor = new Succ(this)
	
	def + (b: Nat): Nat = 
		if(b.isZero) this
		else this.successor + b.predecessor
	
	def - (b: Nat): Nat = 
		if(b.isZero) this
		else this.predecessor - b.predecessor

	def == (b:Nat):Boolean = 
		if(b.isZero) this.isZero
		else this.predecessor == b.predecessor
}

object Zero extends Nat{
	val isZero: Boolean = true
	def predecessor: Nat = throw new UnsupportedOperationException("Zero.predecessor")

	override def toString = "0"
}

class Succ(val predecessor:Nat) extends Nat{
	val isZero: Boolean = false

	override def toString = 
		"i" + (if(predecessor.isZero) "" else predecessor.toString)
}