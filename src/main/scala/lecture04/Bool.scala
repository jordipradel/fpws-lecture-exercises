package lecture04

abstract class Bool{
	def ifThenElse[T](t: => T, e: => T): T

	def && (b: => Bool) = ifThenElse(b, False)
	def || (b: => Bool) = ifThenElse(True,b)
	def unary_! : Bool = ifThenElse(False,True)

	def == (b:Bool) = ifThenElse(b, !b)
	def != (b:Bool) = ifThenElse(!b, b)

	def < (b:Bool) = ifThenElse(False,b)
}

object True extends Bool{
	def ifThenElse[T](t: => T, e: => T) = t
}

object False extends Bool{
	def ifThenElse[T](t: => T, e: => T) = e
}