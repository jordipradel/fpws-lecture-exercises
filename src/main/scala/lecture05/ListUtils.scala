package lecture05

object ListUtils {

	def flatten(l:List[Any]):List[Any] = l match {
		case Nil => Nil
		case (eh :: et) :: t => flatten(eh :: et) ::: flatten(t)
		case e :: t => e :: flatten(t)
	}

	def merge(l1:List[Int],l2:List[Int]):List[Int] = (l1,l2) match {
		case (Nil,_) => l2
		case (_,Nil) => l1
		case (h1::t1,h2::t2) => if (h1<=h2) h1::merge(t1,l2) else h2::merge(l1,t2)
		case _ => throw new UnsupportedOperationException("Not yet implemented")
	}

	def lengthFun[T](l:List[T]) = (l foldRight 0) ((x,acc) => acc+1)

	def mapFun[T,U](l:List[T], f: T=>U): List[U] =
		(l foldRight List[U]())((x,acc) => f(x)::acc)
}