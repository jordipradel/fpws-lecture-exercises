package lecture02

import common._
import scala.annotation.tailrec

object Ex3 {

	def product(f:Int=>Int)(a:Int,b:Int):Int = 
		if ( a > b ) 1 else f(a) * product(f)(a+1,b)

	def factorial(n:Int):Int = product(i=>i)(1,n)

	def mapReduce(m:Int=>Int, combine:(Int,Int)=>Int, zero:Int)(a:Int,b:Int):Int =
		if(a>b) zero
		else combine(m(a),mapReduce(m,combine,zero)(a+1,b))
}