package lecture03

import common._
import scala.math.abs
import scala.annotation.tailrec

object Ex1 {

  class Rational(n:Int, d:Int){
    require(d!=0,"denominator must be nonzero")

    private def gcd(a:Int, b:Int): Int = if(b==0) a else gcd(b,a % b)
    private val g = abs(gcd(n,d));
    val numer = n/g;
    val denom = d/g;

    def this(n:Int) = this(n,1)

    def + (that:Rational) = new Rational(
        numer * that.denom + that.numer * denom,
        denom * that.denom)

    def unary_- = new Rational(-numer,denom)

    def - (that:Rational) = this + - that

    def < (that:Rational):Boolean = this.numer * that.denom < that.numer * this.denom

    def max (that:Rational):Rational = if(this < that) that else this

    override def toString = numer + (if(denom==1) "" else "/"+denom)
  }
}