package lecture03

abstract class IntSet{
	def incl(x:Int): IntSet
	def contains(x:Int): Boolean
	def isEmpty: Boolean
	def union(that:IntSet): IntSet
}

object Empty extends IntSet{
	override def contains(x:Int) = false
	override def incl(x:Int) = new NonEmpty(x, Empty, Empty)
	override def isEmpty = true
	override def union(that:IntSet) = that
	override def toString = "{}"
}

class NonEmpty(elem:Int, left:IntSet, right:IntSet) extends IntSet{
	override def contains(x:Int):Boolean =
		if(x < elem) left contains x
		else if (x > elem) right contains x
		else true
	
	override def incl(x:Int):IntSet = 
		if(x < elem) new NonEmpty(elem, left incl x, right)
		else if (x > elem) new NonEmpty(elem, left, right incl x)
		else this

	override def isEmpty = false

	override def union(that:IntSet) =
		left union right union (that incl elem)

	override def toString = {
		val sLeft = left.toString
		val leftComma = if(left.isEmpty) "" else ","
		val rightComma = if(right.isEmpty) "" else ","
		sLeft.substring(0,sLeft.length - 1) + leftComma + elem + rightComma + right.toString.substring(1)
	}

}