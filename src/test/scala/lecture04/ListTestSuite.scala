package lecture04

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ListTestSuite extends FunSuite {

	test("Nil.isEmpty is true"){
		assert(Nil.isEmpty === true)
	}

	test("Any other list is not empty"){
		val l = new Cons(1, Nil)
		assert(l.isEmpty === false)
	}

	test("Nil.head throws NoSuchElementException"){
		intercept[NoSuchElementException]{
			Nil.head			
		}
	}

	test("Nil.tail throws NoSuchElementException"){
		intercept[NoSuchElementException]{
			Nil.tail			
		}
	}

	test("head of cons is the head of cons"){
		val l = new Cons(1, Nil)
		assert(l.head === 1)
	}

	test("tail of cons is the tail of cons"){
		val nil = Nil
	 	val l = new Cons(1, nil)
	 	assert(l.tail === nil)
	}

	test("List(elem) builds a list with just one element"){
		import List._
		val l = List(4)
		assert(l.head === 4)
		assert(l.tail.isEmpty)
	}

	test("cons conses an element to a list"){
		val l = List(4).cons(5).cons(3)
		assert(l.head === 3)
		assert(l.tail.head === 5)
		assert(l.tail.tail.head === 4)
		assert(l.tail.tail.tail.isEmpty)
	}

	test("nth of nil throws IndexOutOfBoundsException"){
		intercept[IndexOutOfBoundsException]{
			Nil.nth(0)
		}
	}

	test("nth returns the nth element of a list"){
		val l2468 = List(8).cons(6).cons(4).cons(2)
		assert(l2468.nth(0) === 2)
		assert(l2468.nth(1) === 4)
		assert(l2468.nth(2) === 6)
		assert(l2468.nth(3) === 8)
	}

	test("nth with a negative number throws IndexOutOfBoundsException"){
		val l = List(3)
		intercept[IndexOutOfBoundsException]{
			l.nth(-2)
		}
	}

	test("nth with a number greater than the list size throws IndexOutOfBoundsException"){
		val l = List(3)
		intercept[IndexOutOfBoundsException]{
			l.nth(2)
		}
	}

	test("List apply"){
		assert(List().isEmpty)
		val l3 = List(3)
		assert(l3.head === 3)
		assert(l3.tail.isEmpty)
		val l34 = List(3,4)
		assert(l34.head === 3)
		assert(l34.tail.head === 4)
		assert(l34.tail.tail.isEmpty)
	}
}