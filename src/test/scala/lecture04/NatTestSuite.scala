package lecture04

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NatTestSuite extends FunSuite {

	test("zero isZero"){
		assert(Zero.isZero === true)
	}

	test("succ is not zero"){
		assert(new Succ(Zero).isZero === false)
	}

	test("Zero.successor.predecessor is Zero"){
		assert(Zero.successor.predecessor === Zero)
	}

	test("Zero.predecessor throws an exception"){
		val exc = intercept[UnsupportedOperationException]{
			Zero.predecessor
		}
		assert(exc.getMessage === "Zero.predecessor")
	}

	test("Zero == Zero"){
		assert((Zero == Zero) === true)
	}

	test("Zero.successor == Zero.successor"){
		assert((Zero.successor == Zero.successor) === true)
	}

	class Examples {
		val two = Zero.successor.successor
		val three = Zero.successor.successor.successor
		val five = Zero.successor.successor.successor.successor.successor
	}

	test("2 + 3 == 5"){
		new Examples{
			assert((two + three == five) === true)
		}
	}

	test("5 - 3 == 2"){
		new Examples{
			assert((five - three == two) === true)
		}
	}



}