package lecture04

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BoolTestSuite extends FunSuite {

	test("&&"){
		assert((True && True) === True)
		assert((True && False) === False)
		assert((False && True) === False)
		assert((False && False) === False)
	}

	test("||"){
		assert((True || True) === True)
		assert((True || False) === True)
		assert((False || True) === True)
		assert((False || False) === False)
	}

	test("!"){
		assert(!True === False)
		assert(!False === True)
	}

	test("=="){
		assert((True == True) === True)
		assert((True == False) === False)
		assert((False == True) === False)
		assert((False == False) === True)
	}

	test("!="){
		assert((True != True) === False)
		assert((True != False) === True)
		assert((False != True) === True)
		assert((False != False) === False)
	}

	test("<"){
		assert((True < True) === False)
		assert((True < False) === False)
		assert((False < True) === True)
		assert((False < False) === False)
	}

}