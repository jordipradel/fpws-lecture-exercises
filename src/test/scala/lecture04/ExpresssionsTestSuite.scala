package lecture04

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import Expr.{eval,normalize,show}

@RunWith(classOf[JUnitRunner])
class ExpressionsTestSuite extends FunSuite {

	test("eval Num"){
		assert(eval(Number(3)) === 3)
	}

	test("eval Sum"){
		assert(eval(Sum(Number(3), Number(5))) === 8)
	}

	test("+"){
		assert(eval(Number(4) + Number(5)) === 9)
	}

	test("*"){
		assert(eval(Number(2) * Number(5)) === 10)
	}

	test("normalize simple expression"){
		assert(normalize(Number(4)) === Number(4))
	}


	trait SampleExpressions{
		val _2plus3_Times_minus2 = (Number(2)+Number(3)) * Number(-2)
		val _2timesMinus2plus3timesMinus2 = Number(2) * Number(-2) + Number(3) * Number(-2)
		val _2_times_3plus4 = Number(2) * (Number(3) + Number(4))
	}

	test("normalize product of sums"){
		new SampleExpressions{
			assert(normalize(_2plus3_Times_minus2) === _2timesMinus2plus3timesMinus2)
		}
	}

	test("normalize product of sum on the right"){
		new SampleExpressions{
			assert(normalize(_2_times_3plus4) === Number(2) * Number(3) + Number(2) * Number(4))
		}
	}

	test("normalize deeper"){
		new SampleExpressions{
			assert(normalize(Number(3) + _2plus3_Times_minus2) === Number(3) + _2timesMinus2plus3timesMinus2)
		}
	}

	test("show"){
		new SampleExpressions{
			assert(show(_2_times_3plus4) === "2 * (3 + 4)")
		}
	}
}