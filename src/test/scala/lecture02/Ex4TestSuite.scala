package lecture02

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Ex4TestSuite extends FunSuite {

	import Ex4._

	test("the new version of sqrt should work like the first one"){
		assert(sqrt2(2) === sqrt(2))
	}

}