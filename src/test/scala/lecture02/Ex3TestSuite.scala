package lecture02

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Ex3TestSuite extends FunSuite {

	import Ex3._

	test("product of incs works"){
		assert(product(i=>i+1)(1,3) === 2*3*4)
	}

	test("factorial of 5 is 120"){
		assert(factorial(5) === 120)
	}

	test("map can build a function like product"){
		assert(mapReduce(i=>i+1,(i,j)=>i*j,1)(1,3) === product(i=>i+1)(1,3))
	}

	test("product can be defined using mapReduce"){
		// As in the lecture
		def product2(f:Int=>Int)(a:Int,b:Int):Int = mapReduce(f,(i,j)=>i*j,1)(a,b)
		assert(product2(i=>i+1)(1,3) === 2*3*4)

		// Or, shorter...
		def product3(f:Int=>Int) = mapReduce(f,(i,j)=>i*j,1)_
		assert(product3(i=>i+1)(1,3) === 2*3*4)
	}

}