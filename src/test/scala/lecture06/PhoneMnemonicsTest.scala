package lecture06

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import PhoneMnemonics._

@RunWith(classOf[JUnitRunner])
class PhoneMnemonicsTestSuite extends FunSuite {

  test("char code"){
    assert(charCode('A') === '2')
    assert(charCode('B') === '2')
    assert(charCode('D') === '3')
  }


  test("word code"){
    assert(wordCode("Java") === "5282")
  }

  test("words for num"){
    assert(wordsForNum("5282").contains("Java"),"Java should be in " + wordsForNum("5282"))
  }

  test("encode"){
    val quants = 15
    println("Codis: " + charCode)
    //assert(translations.contains("Scala is fun"), "Scala is fun found in " + translations)
    println("Casa: " + translate("935998846").take(quants).mkString(" - "))
    println("Vanessa: " + translate("625641599").take(quants).mkString(" - "))
    println("Jordi: " + translate("675837541").take(quants).mkString(" - "))
    println("Pares Vanessa: " + translate("935622932").take(quants).mkString(" - "))
    println("Pare Jordi: " + translate("933574836").take(quants).mkString(" - "))
    println("Marc mòvil: " + translate("650404960").take(quants).mkString(" - "))

  }

}
