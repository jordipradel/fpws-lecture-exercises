
import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class KataTestSuite extends FunSuite {

	var a = List(1,2,3)

	def deepSearch(tree:List[Int]) = {
		if (tree.length <= 1) tree else {
			List(hijoDerecho(tree), tree(0), hijoIzquierdo(tree)) 
		}
	}

	def hijoIzquierdo(tree:List[Int])  = tree(1)

	def hijoDerecho(tree:List[Int]) = tree(tree.length-1)

	test("Retorna vacio con arbol vacio") {
		assert(deepSearch(List()) === List(),"Recorrer el arbol vacio da ''")
	}
	test("Retorna nodo si arbol es un unico nodo") {
		assert(deepSearch(List(1)) === List(1), "Recorrer un arbol de un nodo da el nodo")
	}
	test("Retorna el recorrido en postorden de un arbol de 3 nodos"){
		assert(deepSearch(List(1,2,3)) === List(3,1,2))
	} 

}