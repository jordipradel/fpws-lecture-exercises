package lecture03

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Ex1TestSuite extends FunSuite {

  import Ex1._

  trait SampleRationals {
    val oneThird = new Rational(1,3)
    val oneFourth = new Rational(1,4)
  }

  test("one third plus one fourth equals one sixth"){
    new SampleRationals{
      assert((oneThird + oneFourth).toString === "7/12")
    }
  }

  test("neg of 1/3 is -1/3"){
    new SampleRationals{
      val minusOneThird = - oneThird
      assert(minusOneThird.toString === "-1/3")
    }
  }

  test("1/3 - 1/4 is 1/12"){
    new SampleRationals{
      val result = oneThird - oneFourth
      assert(result.numer === 1)
      assert(result.denom === 12)
    }
  }

  test("exercise response"){
    val x = new Rational(1,3)
    val y = new Rational(5,7)
    val z = new Rational(3,2)
    val result = x - y - z;
    assert(result.numer === -79)
    assert(result.denom === 42)
  }

  test("rationals are normalized"){
    val twoSixths = new Rational(2,6)
    assert(twoSixths.toString === "1/3")
  }

  test("1/4 is less than 1/3"){
    new SampleRationals{
      assert(oneFourth < oneThird)
    }
  }

  test("max of 1/4 and 1/3 is 1/3"){
    new SampleRationals{
      assert((oneThird max oneFourth).toString === "1/3")
    }
  }

  test("0 denominator not allowed"){
    intercept[IllegalArgumentException]{
      new Rational(1,0)
    }
  }

  test("You can use a one argumetn constructor"){
    val two = new Rational(2)
    assert(two.numer === 2)
    assert(two.denom === 1)
  }

  test("Integer rational 2/1 is printed as 2"){
    assert(new Rational(2).toString === "2")
  }

}