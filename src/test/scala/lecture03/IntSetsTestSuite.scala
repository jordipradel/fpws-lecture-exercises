package lecture03

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class IntSetsTestSuite extends FunSuite {

  test("empty set doesn't include int 3"){
    val empty = Empty
    assert(!(empty contains 3))
  }

  test("a set with only one number contains that number"){
    val three = Empty incl 3
    assert(three.contains(3))
  }

  test("empty incl 3 incl 7 includes both 3 and 7"){
    val threeAndSeven = Empty incl 3 incl 7
    assert(threeAndSeven contains 3)
    assert(threeAndSeven contains 7)
  }

  test("empty toString is {}"){
    assert(Empty.toString === "{}")
  }

  test("empty incl 3 toString is {3}"){
    assert((Empty incl 3).toString === "{3}")
  }

  test("empty incl 5 incl 3 is {3,5}"){
    assert((Empty incl 5 incl 3).toString === "{3,5}")
  }

  test("empty union empty is empty"){
    assert((Empty union Empty).toString === "{}")
  }

  test("empty union {3,7} is {3,7}"){
    val threeAndSeven = Empty incl 3 incl 7
    assert((Empty union threeAndSeven).toString === "{3,7}")
  }

  test("{3,7} union {} is {3,7"){
    val threeAndSeven = Empty incl 3 incl 7
    assert((threeAndSeven union Empty).toString === "{3,7}")
  }

  test("{3,7} union {1,5} is {1,3,5,7"){
    val threeAndSeven = Empty incl 3 incl 7
    val oneAndFive = Empty incl 1 incl 5
    assert((threeAndSeven union oneAndFive).toString === "{1,3,5,7}")
  }

}