package lecture05

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import ListUtils.{flatten,merge,lengthFun,mapFun}

@RunWith(classOf[JUnitRunner])
class ListUtilsTestSuite extends FunSuite {

	test("flatten the empty list"){
		assert(flatten(Nil) === Nil)
	}

	test("flatten a flat list"){
		assert(flatten(1::2::3::Nil) === 1::2::3::Nil)
	}

	test("flatten a complex list"){
		val res = flatten(List(List(1,1),2,List(3,List(5,8))))
		assert(res === List(1,1,2,3,5,8))
	}

	test("merge an empty list with a list"){
		assert(merge(Nil,1::2::Nil) === 1::2::Nil)
	}

	test("merge a list with an empty list"){
		assert(merge(1::2::Nil,Nil) === 1::2::Nil)
	}

	test("merge with the head of a list as head of the result"){
		assert(merge(1::Nil,2::3::Nil) === 1::2::3::Nil)
		assert(merge(2::3::Nil,1::Nil) === 1::2::3::Nil)
	}

	test("merge 2 lists, complex case"){
		assert(merge(1::3::4::8::Nil,2::4::6::Nil) === 1::2::3::4::4::6::8::Nil)
	}

	test("lengthFun"){
		assert(lengthFun(Nil) === 0)
		assert(lengthFun(1::2::3::Nil) === 3)
	}

	test("mapFun"){
		assert(mapFun(Nil,(x:Int)=>x*2) === Nil)
		assert(mapFun(2::3::7::Nil,(x:Int)=>x*2) === 4::6::14::Nil)
	}
}