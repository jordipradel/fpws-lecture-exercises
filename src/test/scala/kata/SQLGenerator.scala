package kata

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner


@RunWith(classOf[JUnitRunner])
class SQLGenerator extends FunSuite {

  test("simple select"){
    val res = select("table1")
    assert(res === "select * from table1") 
  }

  def select(tableName: String) = "select * from " + tableName

}